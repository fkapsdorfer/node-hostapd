/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license MIT
 * @file This file loads and exports the configurations.
 */

'use strict'

var fs = require('fs');


var configStr = fs.readFileSync('config.json', { flag: 'r' });
var CONFIG = JSON.parse(configStr.toString());

CONFIG.GLOBAL_ROOT = CONFIG.HTTPD.PROTOCOL + '://' + CONFIG.WLAN.GATEWAY + ':' + CONFIG.HTTPD.PORT;
CONFIG.STDOUT = process.argv[2];


module.exports = CONFIG;
