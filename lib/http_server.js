/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license MIT
 * @file This file defines and exports an http server.
 */

'use strict';

var http 	= require('http');
var server 	= http.createServer();


module.exports = server;
