/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license MIT
 * @file This file defines and exports the base functionality.
 */

'use strict'

var exec 	= require('child_process').exec;
var swig 	= require('swig');
var fs 		= require('fs');
var CONFIG 	= require('./config');




var app = {};

app._shutdownTimer;
app._shutdownTime;
app._suspendTimer;
app._suspendTime;


app.exit = function() {
	console.log('exiting...');
	app.restoreDhcpdConf();
	app.restoreHostapdConf();
	app.restoreHostapdAccept();
	app.stop().then(function() {
		// logged
		process.exit(0);
	}).catch(function() {
		// logged
	});
}

/**
 * Called by app.exit().
 */
app.stop = function() {
	var script = swig.renderFile('./lib/shell.sh.tmpl', { CONFIG: CONFIG, ACTION: 'stop' });

	return new Promise(function(res, rej) {
		console.log('stopping...');

		exec(script, function(err) {
			if (err) {
				console.error('ERROR: cannot stop, see stdout/stderr below');
				console.log(stdout);
				console.log(stderr);
				rej(err);
			} else {
				console.log('down');
				res();
			}
		});
	});
}

app.start = function() {
	var script = swig.renderFile('./lib/shell.sh.tmpl', { CONFIG: CONFIG, ACTION: 'start' });

	return new Promise(function(res, rej) {
		Promise.all([
			app.isDhcpdRunning(),
			app.isHostapdRunning()
		]).then(function(running) {
			if(running[0] || running[1]) {
				return app._restart();
			} else {
				console.log('starting...');

				exec(script, function(err, stdout, stderr) {
					if (err) {
						console.error('ERROR: cannot start, see stdout/stderr below');
						console.log(stdout);
						console.log(stderr);
						rej(err);
					} else {
						console.log('running');
						res();
					}
				});
			}
		});
	});
}

app._restart = function() {
	return app.stop().then(app.start);
}

app.restart = function() {
	exec('sleep 2 && npm start ' + CONFIG.STDOUT);
	app.exit();
}

app.isDhcpdRunning = function() {
	return new Promise(function(res, rej) {
		exec('ps -e | grep dhcpd', function(err, stdout, stderr) {
			if (err) {
				return res(false);
			} else {
				return res(true);
			}
		});
	});
}

app.isHostapdRunning = function() {
	return new Promise(function(res, rej) {
		exec('ps -e | grep hostapd', function(err, stdout, stderr) {
			if (err) {
				return res(false);
			} else {
				return res(true);
			}
		});
	});
}



/**
 * COMPUTER SHUTDOWN, SUSPEND
 */

app._shutdownComputer = function() {
	console.log('shutting down in 5s (+60s)...');
	exec('sleep 5 && shutdown -P 1', function(err, stdout, stderr) {
		if (err) {
			console.log('err');
		} else {
			console.log('ok');
		}
	});
	app.exit();
}

app._suspendComputer = function() {
	console.log('suspending in 5s...');
	exec('sleep 5 && pm-suspend', function(err, stdout, stderr) {
		if (err) {
			console.log('err');
		} else {
			console.log('ok');
		}
	});
	app.exit();
}

app.setShutdownTimer = function(minutes) {
	var ms = minutes*60*1000;

	app.unsetSuspendTimer();
	clearTimeout(app._shutdownTimer);
	app._shutdownTimer = setTimeout(app._shutdownComputer, ms);
	app._shutdownTime = new Date(Date.now() + ms);
}

app.unsetShutdownTimer = function() {
	clearTimeout(app._shutdownTimer);
	app._shutdownTimer = null;
	app._shutdownTime = null;
}

app.setSuspendTimer = function(minutes) {
	var ms = minutes*60*1000;

	app.unsetShutdownTimer();
	clearTimeout(app._suspendTimer);
	app._suspendTimer = setTimeout(app._suspendComputer, ms);
	app._suspendTime = new Date(Date.now() + ms);
}

app.unsetSuspendTimer = function() {
	clearTimeout(app._suspendTimer);
	app._suspendTimer = null;
	app._suspendTime = null;
}


app._createConfig = function(etcConfFile, tmplFile, origConfFile) {
	return new Promise(function(res, rej) {
		fs.readFile(etcConfFile, function(err, data) {
			if (err) {
				console.info('No previous configuration of ' + etcConfFile + ' or no permission.');
				//return rej(err);
			}
			fs.writeFile(origConfFile, data, function(err) {
				if (err) {
					return rej(err);
				}
				fs.writeFile(etcConfFile, swig.renderFile(tmplFile, CONFIG), function(err) {
					if (err) {
						return rej(err);
					} else {
						return res();
					}
				})
			});
		});
	});
}

app._restoreConfig = function(origConfFile, etcConfFile) {
	var data = fs.readFileSync(origConfFile);
	fs.writeFileSync(etcConfFile, data);
}


app.configDhcpd 		= app._createConfig.bind(null, CONFIG.CONF.ETC.DHCPD, CONFIG.CONF.TMPL.DHCPD, CONFIG.CONF.ORIG.DHCPD);
app.restoreDhcpdConf 	= app._restoreConfig.bind(null, CONFIG.CONF.ORIG.DHCPD, CONFIG.CONF.ETC.DHCPD);

app.configHostapd 		= app._createConfig.bind(null, CONFIG.CONF.ETC.HOSTAPD, CONFIG.CONF.TMPL.HOSTAPD, CONFIG.CONF.ORIG.HOSTAPD);
app.restoreHostapdConf 	= app._restoreConfig.bind(null, CONFIG.CONF.ORIG.HOSTAPD, CONFIG.CONF.ETC.HOSTAPD);

app.configHostapdAccept 	= app._createConfig.bind(null, CONFIG.CONF.ETC.ACCEPT, CONFIG.CONF.TMPL.ACCEPT, CONFIG.CONF.ORIG.ACCEPT);
app.restoreHostapdAccept 	= app._restoreConfig.bind(null, CONFIG.CONF.ORIG.ACCEPT, CONFIG.CONF.ETC.ACCEPT);




module.exports = app;
