#!/bin/bash

#
# @author Filip Kapsdorfer
# @copyright Filip Kapsdorfer 2015
# @license MIT
# @file This file is a template or interface for system calls.
#

#
# STEPS:
# setup WIFI - hostapd
# setup DHCP - dhcpd (isc-dhcp-server)
# setup NAT - iptables - forward, nat
#

DHCPD_CONF={{ CONFIG.CONF.ETC.DHCPD }}
HOSTAPD_CONF={{ CONFIG.CONF.ETC.HOSTAPD }}
WLAN_IF={{ CONFIG.WLAN_IF.NAME }}
ETH_IF={{ CONFIG.ETH_IF.NAME }}
WLAN_MAC_ON={{ CONFIG.WLAN.GATEWAY_MAC }}
WLAN_MAC_OFF=`ethtool -P wlan0 | awk '{print $3}'`
WLAN_IF_IP={{ CONFIG.WLAN.GATEWAY }}
WLAN_NETMASK={{ CONFIG.WLAN.NETMASK }}
ACTION={{ ACTION }}


stop() {
	killall dhcpd
	killall hostapd

	ifconfig $WLAN_IF down
	sleep 1
	ifconfig $WLAN_IF hw ether $WLAN_MAC_OFF

	flush_iptables
}



start() {
	# a workaround for a bug/issue:
	# rfkill: WLAN soft blocked
	# rfkill: WLAN hard blocked
	# Could not set interface wlan0 flags (UP): Operation not possible due to RF-kill
	# wlan0: Could not connect to kernel driver
	sudo nmcli radio wifi off
	sudo rfkill unblock wlan
	# might want to use 'nm' instead of 'radio' in older versions of nmcli
	# sudo nmcli nm wifi off

	ifconfig $WLAN_IF down
	sleep 1
	ifconfig $WLAN_IF hw ether $WLAN_MAC_ON
	ifconfig $WLAN_IF $WLAN_IF_IP netmask $WLAN_NETMASK
	ifconfig $WLAN_IF up

	flush_iptables
	setup_iptables

	dhcpd -cf $DHCPD_CONF $WLAN_IF
	hostapd $HOSTAPD_CONF
}



flush_iptables() {
	iptables --flush
	iptables --table nat --flush
	iptables --delete-chain
	iptables --table nat --delete-chain

	echo 0 > /proc/sys/net/ipv4/ip_forward
}

# setup nat
setup_iptables() {
	iptables --table nat --append POSTROUTING --out-interface $ETH_IF -j MASQUERADE
	iptables --append FORWARD --in-interface $WLAN_IF -j ACCEPT

	# allow nat
	echo 1 > /proc/sys/net/ipv4/ip_forward
}




case $ACTION in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		stop
		start
		;;
	*)
		echo 'start|stop|restart'
		exit 1
		;;
esac
