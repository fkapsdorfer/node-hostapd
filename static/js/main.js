/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license MIT
 * @file This is the main fronted script.
 */

'use strict';



var app = {
	//dom: {}
};


var util = {
	q: document.querySelector.bind(document),
	on: function(el, evt, handler) {
		el.addEventListener(evt, handler);
	},
	get: function(query) {
		return new Promise(function(res, rej) {
			var url = window.GLOBAL_ROOT + query;
			var xhr = new XMLHttpRequest();

			xhr.onreadystatechange = function() {
			    if (xhr.readyState == 4 && xhr.status == 200) {
					var str = xhr.responseText;
					if (str != '') {
			        	res(JSON.parse(xhr.responseText));
					} else {
						res(null);
					}
			    }
			}

			xhr.open("GET", url, true);
			xhr.send();
		});
	}
};



(function() {
	var q 	= util.q;
	var on 	= util.on;
	var get = util.get;

	/**
	 * Call this when the window is loaded.
	 */
	app.init = function() {
		app.initSelectors();
		app.initListeners();
		app.dom.btnGetStatus.click();
	}

	app.initSelectors = function() {
		app.dom = {
			result 					: q('#result'),
			btnGetStatus 			: q('#btnGetStatus'),
			btnGetConfig 			: q('#btnGetConfig'),
			btnRestart 				: q('#btnRestart'),
			btnStop 				: q('#btnStop'),
			inpShutdownTimer 		: q('#inpShutdownTimer'),
			btnSetShutdownTimer 	: q('#btnSetShutdownTimer'),
			btnUnsetShutdownTimer 	: q('#btnUnsetShutdownTimer'),
			inpSuspendTimer 		: q('#inpSuspendTimer'),
			btnSetSuspendTimer 	: q('#btnSetSuspendTimer'),
			btnUnsetSuspendTimer 	: q('#btnUnsetSuspendTimer')
		}
	}

	app.initListeners = function() {
		on(app.dom.btnGetStatus, 'click', function() {
			get('/status').then(function(data) {
				app.printResult(data);
			});
		});
		on(app.dom.btnGetConfig, 'click', function() {
			get('/config').then(function(data) {
				app.printResult(data);
			});
		});
		on(app.dom.btnRestart, 'click', function() {
			get('/restart').then(function(data) {
				app.dom.result.textContent = 'wait...';
				setTimeout(function() {
					//app.dom.btnGetStatus.click();
					window.location.reload();
				}, 3000);
			});
		});
		on(app.dom.btnStop, 'click', function() {
			get('/stop').then(function(data) {
				window.location.reload();
			});
		});
		on(app.dom.btnSetShutdownTimer, 'click', function() {
			get('/setShutdownTimer/' + app.dom.inpShutdownTimer.value).then(function(data) {
				app.dom.btnGetStatus.click();
			});
		});
		on(app.dom.btnUnsetShutdownTimer, 'click', function() {
			get('/unsetShutdownTimer').then(function(data) {
				app.dom.btnGetStatus.click();
			});
		});
		on(app.dom.btnSetSuspendTimer, 'click', function() {
			get('/setSuspendTimer/' + app.dom.inpSuspendTimer.value).then(function(data) {
				app.dom.btnGetStatus.click();
			});
		});
		on(app.dom.btnUnsetSuspendTimer, 'click', function() {
			get('/unsetSuspendTimer').then(function(data) {
				app.dom.btnGetStatus.click();
			});
		});


	}

	app.printResult = function(obj) {
		app.dom.result.textContent = JSON.stringify(obj, null, 4);
	}

})();



window.addEventListener('load', function() {
	app.init();
});
