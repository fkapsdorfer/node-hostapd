
Caution
-------

- Read the whole readme
- This is not a deployment ready or maintained application
- Use at your own risk
- MIT license (open-source/free)



Requirements
------------

- unix-like system
- Node.js with npm
- hostapd
- isc-dhcp-server
- iptables
- supported hardware/driver



Notes
-----

- Developed on Ubuntu 14/15, other machines might need some adjustments
- All iptable records will be purged
- `/etc/dhcp/dhcpd.config`, `/etc/hostapd/hostapd.conf`, `/etc/hostapd/accept` will be rewritten (the configurations will be reset the to previous state when application successfully terminates, however keep a backup if necessary)
- The web dashboard is not password protected (expects you not to grant wifi credential to just anybody), therefore everybody connected to the hotspot is able to use its functionality (shut down your computer, ...)



Running The Utility
-------------------

Grant the root privileges!

#### Start:
```sh
sudo npm start [stdout/stderr]
# e.g.
sudo npm start /var/log/node-hostapd.log
sudo npm start /proc/12345/fd/0
sudo npm start
```
#### Stop:
To stop the service use the web dashboard. Do not interrupt/kill the process (^c).



Web Dashboard
-------------

Access via a web browser:
```
http://<wlan.gateway>:<httpd.port>
```
Gateway and port can be found in the configuration file. E.g. (default):
```
http://10.15.0.1:8080
```

![](https://gitlab.com/fkapsdorfer/node-hostapd/raw/master/doc/images/web_dashboard.png)



Configurations
--------------

See `config.json`.



Figure Out Driver Support
-------------------------

```sh
lspci -k | grep -A 3 -i "network"
```

look for kernel driver in use

```sh
modinfo <kernel-driver> | grep 'depend'
```

must depend on one of the following (supported): ***ath9k_hw,mac80211, ath9k_common, ath, cfg80211***





Further Reading
---------------


### Known Issues, Compatibility Issues

This snippet of code is located in `lib/shell.sh.tmpl`.
```sh
# a workaround for a bug/issue:
# rfkill: WLAN soft blocked
# rfkill: WLAN hard blocked
# Could not set interface wlan0 flags (UP): Operation not possible due to RF-kill
# wlan0: Could not connect to kernel driver
sudo nmcli radio wifi off
sudo rfkill unblock wlan
# might want to use 'nm' instead of 'radio' in older versions of nmcli
# sudo nmcli nm wifi off
```



### To Do

- Check if the root privileges given
- Check if the dependencies present
- Protect the dashboard using password or ACL
