/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license MIT
 * @file This file defines and exports an express application.
 */

'use strict'

var fs 		= require('fs');
var express = require('express');
var eapp 	= express();
var app 	= require('./lib/app');
var CONFIG 	= require('./lib/config');
var swig 	= require('swig');


eapp.use(function(req, res, next){
	// overwrite rendering
	res.render = function(viewName) {
		fs.readFile('./views/' + viewName + '.html', {}, function(err, data){
			if (err) {
				res.end();
				console.error('Cannot read a view: ' + viewName);
			} else {
				res.end(data);
			}
		});
	}

	next();
});

eapp.use('/static', express.static(__dirname + '/static'));
eapp.use('/libs', express.static('/srv/www/libs.local'));



eapp.use('/status', function(req, res) {
	var obj = {};

	obj.SHUTDOWN_TIME = app._shutdownTime;
	obj.SUSPEND_TIME = app._suspendTime;

	Promise.all([
		app.isDhcpdRunning(),
		app.isHostapdRunning()
	]).then(function(resArr) {
		obj.DHCPD_RUNNING 	= resArr[0];
		obj.HOSTAPD_RUNNING = resArr[1];

		// RESPOND
		res.end(JSON.stringify(obj));
	});
});

eapp.use('/config', function(req, res) {
	res.end(JSON.stringify(CONFIG));
});

eapp.use('/setShutdownTimer/:minutes', function(req, res) {
	app.setShutdownTimer(req.params.minutes);
	res.end();
});
eapp.use('/unsetShutdownTimer', function(req, res) {
	app.unsetShutdownTimer();
	res.end();
});

eapp.use('/setSuspendTimer/:minutes', function(req, res) {
	app.setSuspendTimer(req.params.minutes);
	res.end();
});
eapp.use('/unsetSuspendTimer', function(req, res) {
	app.unsetSuspendTimer();
	res.end();
});

eapp.use('/restart', function(req, res) {
	res.end();
	app.restart();
});

eapp.use('/stop', function(req, res) {
	res.end();
	app.exit();
});

eapp.use('/', express.Router()
	.get('/', function(req, res) {
		res.end(swig.renderFile('./views/home.html', CONFIG));
	})
	.get('*', function(req, res) {
		res.status(404).end(JSON.stringify({ error: true, msg: 'Not Found' }));
	})
);



module.exports = eapp;
